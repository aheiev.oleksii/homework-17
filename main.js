function main(a = 2, b = 3, c = null) {
  if (typeof c === "function") {
    return c(a, b);
  }
  return sum(a, b);
}

function sum(a, b) {
  return a + b;
}

console.log(main(5, 5, sum));
console.log(main(7, 4));
